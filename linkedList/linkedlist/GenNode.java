
package linkedlist;
/**
 * Klasse zum Erstellen eines Listenknotens.
 * @author Micha
 * @param <T> generischer Datentyp
 */
public class GenNode <T> {
    
    private GenNode nextNode;
    private GenNode lastNode;
    private T value;
    
    /**
     * Node-Konstruktor.     
     * @param lastNode Referenz zum vorherigen Knoten.
     * @param nextNode Referenz zum naechsten Knoten
     * @param value Wert des Strings den der Knoten beinhaelt
     */  
    
    public GenNode(GenNode lastNode, GenNode nextNode, T value) {
        this.nextNode = nextNode;
        this.lastNode = lastNode;
        this.value = value;              
    }
    
    public GenNode getNextNode() {
        return this.nextNode;
    }
    
    public GenNode getLastNode() {
        return this.lastNode;
    }
    
    public T getValue() {
        return this.value;
    }
    
    public void setNextNode(GenNode nextNode) {
        this.nextNode = nextNode;
    }
    
    public void setLastNode(GenNode lastNode) {
        this.lastNode = lastNode;
    }
    
    
}