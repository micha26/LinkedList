
package linkedlist;

import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedListTest {
    

    private GenLinkedList testIntList = new GenLinkedList();
    private GenLinkedList testDoubleList = new GenLinkedList();
    private GenLinkedList testStringList = new GenLinkedList();

    @Test
    public void testStringAddFirst() {
        testStringList.addFirst("Hallo");
        assertEquals("Hallo", testStringList.get(1));
    }

    @Test
    public void testGenAddFirst() {
    	testIntList.addFirst(15);
    	assertEquals(15, testIntList.get(1));
    }
    

	@Test
    public void testGenAddFirst1() {
    	testDoubleList.addFirst(15.44);
    	assertEquals(15.44, testDoubleList.get(1));
    }

    @Test
    public void testAddFirst() {
        testIntList.addFirst("testFirst");
        assertEquals("testFirst", testIntList.get(1));
    }

    @Test
    public void testAddLast() {
        testIntList.addLast("testLast");
        assertEquals("testLast", testIntList.get(1));
    }

    @Test
    public void testAdd() {
        testIntList.addFirst("nr1");
        testIntList.addFirst("nr2");
        testIntList.addFirst("nr3");
        testIntList.add(3, "neueNr1");
        assertEquals("neueNr1", testIntList.get(3));
    }

    @Test
    public void testGet() {
        testIntList.addFirst("nr1");
        testIntList.addFirst("nr2");
        testIntList.addFirst("nr3");
        assertEquals("nr2", testIntList.get(2));
    }
   
    @Test
    public void testRemoveFirst() {
        testIntList.addFirst("nr1");
        testIntList.addFirst("nr2");
        testIntList.addFirst("nr3");
        testIntList.removeFirst();
        assertEquals(2, testIntList.getSize());
    }

    @Test
    public void testRemoveLast() {
        testIntList.addLast("nr1");
        testIntList.addLast("nr2");
        testIntList.addLast("nr3");
        testIntList.removeLast();
        assertEquals(2, testIntList.getSize());
    }

    @Test
    public void testGetSize() {
        testIntList.addFirst("nr1");
        testIntList.addFirst("nr2");
        testIntList.addFirst("nr3");
        testIntList.add(3, "neueNr1");
        assertEquals(4, testIntList.getSize());
    }
    
    @Test
    public void testAddNegative() {
        testIntList.add(-5, "nr1");
        assertEquals("nr1", testIntList.get(1));
    }

    @Test
    public void testAddTooHigh() {
        testIntList.addFirst("nr1");
        testIntList.add(5, "nr2");
        assertEquals("nr2", testIntList.get(2));
    }    
    
    @Test
    public void testRemoveEmptyList() {
        testIntList.removeFirst();
        assertEquals(0, testIntList.getSize());
    }    

    @Test
    public void testRemoveEmptyList2() {
        testIntList.removeLast();
        assertEquals(0, testIntList.getSize());
    }    
}
