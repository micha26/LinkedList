
package linkedlist;

/**
 * Klasse zum Erstellen einer generischen doppelt verketteten Liste.
 * @author Micha
 *
 * @param <T> generischer Datentyp
 */
public class GenLinkedList <T> {
    
    private GenNode node;
    private GenNode currentNode;
    private int counter = 0;

    /**
     * Bei leerer Liste - erstelle ersten Listenknoten.
     * Bei gefuellter Liste - fuege Knoten an erster Stelle ein
     * @param value entspricht dem String des Knotens
     */
    public void addFirst(T value) {
        
        if (getSize() == 0) {
            node = new GenNode(null, null, value);
            counter++;
        }       
        else {
            getNode("first");
            node = new GenNode(null, currentNode, value);
            node.getNextNode().setLastNode(node);            
            counter++;
        }           
    }
    
     /**
     * Bei leerer Liste - erstelle ersten Listenknoten.
     * Bei gefuellter Liste - fuege Knoten an letzter Stelle ein
     * @param value entspricht dem String des Knotens
     */   
    public void addLast(T value) {       
        
        if (getSize() == 0) {
            node = new GenNode(null, null, value);
            counter++;
        }       
        else {
            getNode("last");
            node = new GenNode(currentNode, null, value);  
            node.getLastNode().setNextNode(node);         
            counter++;
        }       
    }
    
    /**
     * Bei leerer Liste - erstelle ersten Listenknoten.
     * Bei gefuellter Liste - fuege Knoten an Index ein
     * @param index zeigt an welcher Stelle der Knoten eingefuegt werden soll
     * @param value entspricht dem String des Knotens
     */
    public void add(int index, T value) {
        
        if (index < 2) {
            addFirst(value);
        } 
        else if (index > getSize()) {
            addLast(value);
        } 
        else {
            getNode("first");
            for (int i = 1; i < index; i++) {
                currentNode = currentNode.getNextNode();
            }
            node = new GenNode(currentNode.getLastNode(), currentNode, value);
            currentNode.getLastNode().setNextNode(node);
            currentNode.setLastNode(node); 
            counter++;
        }
    }
    
    /**
     * Bestimmt den String eines Knotens am uebergebenen Index.
     * @param index zeigt den gesuchten Knoten
     * @return gibt den String des gesuchten Knotens zurueck
     */
    public T get(int index) {
        
        T value = null;
        getNode("first");
        
        assert index > 0 && index <= getSize();
        
        if (index == 1) {                   
            value = (T) currentNode.getValue();                   
        } 
        else {
            for (int i = 1; i < index; i++) {
                currentNode = currentNode.getNextNode();
                if (i == index - 1) {
                    value = (T) currentNode.getValue();
                }
            }            
        }                                                     
        return value;        
    }
    
    /**
     * entferne ersten Knoten.
     * @return null
     */
    public T removeFirst() {
        
        getNode("first");
        if (currentNode == null) {
            return null;
        }
        currentNode.getNextNode().setLastNode(null);        
        counter--;
        return null;
    }
    
    /**
     * entferne letzten Knoten.
     * @return letzter Knoten
     */
    public T removeLast() {
        
        getNode("last");
        if (currentNode == null) {
            return null;
        }        
        currentNode.getLastNode().setNextNode(null);        
        counter--;
        return null;        
    }
    
    /**
     * bestimme Anzahl der Listenknoten.
     * @return Anzahl der Listenknoten
     */
    public int getSize() {       
        return counter;        
    }      
    
    /**
     * Hilfsmethode bestimmt ersten oder letzten Listenknoten.
     * @param position "first" fuer ersten Knoten und "last" fuer letzten Knoten
     * @return erster Listenknoten
     */
    public GenNode getNode(String position) {
        
        currentNode = node;
        if (getSize() < 1) {
            return currentNode;
        }                
        if (position.equals("first")) {
            while (currentNode.getLastNode() != null) {               
                currentNode = currentNode.getLastNode(); 
            }
        } 
        else if (position.equals("last")) {          
                while (currentNode.getNextNode() != null) {
                currentNode = currentNode.getNextNode();
            }
        }
        return currentNode;
    }
}